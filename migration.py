from flask import Blueprint, request, url_for, jsonify
from flask_cors import cross_orgin, CORS
from sqlalchemy.orm import sessionmaker
from .model import Data, engine

from datetime import datetime
from threading import Thread
import paramiko
import uuid
import json
import os

#for email
from email.mime.text import MIMEtext
from email.mine.multipart import MIMEMultipart
import smtplib
import mimetypes

migration = Blueprint('migration', __name__)
CORS(migration)
Session = sessionmaker(engine)

with open('config.json') as con:
    config = json.load(con)
def send_email(source,message):
    msg = MIMEMultipart()
    msg['Subject'] = 'Migration of {} is completed'.format(source)
    msg['From'] = 'sabeel@xilinx.com'
    msg['To'] = 'sabeel@xilinx.com'
    body = 'Data migration has sucessfully completed'
    msg.attach(MIMEText(body))
    server.sendmail(msg['From'],msg['To'],msg.as_string())
    server.quit()
    

def convert_size(size, typ='G'):
    actual = size[-1]
    if actual == typ:
        return float(size[:-1])
    if typ == 'K' and actual == 'T':
        return float(size[:-1]) * 1024 * 1024 * 1024
    elif typ == 'K' and actual == 'G':
        return float(size[:-1]) * 1024 * 1024
    elif typ == 'K' and actual == 'M':
        return float(size[:-1]) * 1024
    elif typ == 'M' and actual == 'T':
        return float(size[:-1]) * 1024 * 1024
    elif typ == 'M' and actual == 'G':
        return float(size[:-1]) * 1024
    elif typ == 'M' and actual == 'K':
        return float(size[:-1]) / 1024
    elif typ == 'G' and actual == 'T':
        return float(size[:-1]) * 1024
    elif typ == 'G' and actual == 'M':
        return float(size[:-1]) / 1024
    elif typ == 'G' and actual == 'K':
        return float(size[:-1]) / (1024 * 1024)
    elif typ == 'T' and actual == 'G':
        return float(size[:-1]) / 1024
    elif typ == 'T' and actual == 'M':
        return float(size[:-1]) / (1024 * 1024)
    elif typ == 'T' and actual == 'K':
        return float(size[:-1]) / (1024 * 1024 * 1024)


def rsync(pb):
    session = Session()
    data = session.query(Data).filter_by(public_id=pb).first()
    # data.status = 'done'
    # session.commit()
    # print(data.public_id, data.source_path)
    # return data.source_path
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    key = paramiko.RSAKey.from_private_key_file(
        config[data.site_of_primary_archive]['ssh_key'])
    ssh.connect(config[data.site_of_primary_archive]['ip'], pkey=key,
                username=config[data.site_of_primary_archive]['username'])
    cmd = f'cd {data.source_path};du --exclude=.snapshot -sh .'
    inp, out, er = ssh.exec_command(cmd)
    output, error = out.read().decode().strip(), er.read().decode().strip()
    if error:
        data.error = "Can't able to check the source size " + str(error)
	send_email(data.source_path,data.error)
        data.status = 'Error'
        session.commit()
        return
    source_size = convert_size(output.split()[0])
    print('Source Size:', source_size, 'GB')
    for path in config['archive'][data.site_of_primary_archive]:
        cmd = 'df -h ' + path
        inp, out, er = ssh.exec_command(cmd)
        result = out.read().decode().strip().split('\n')
        if len(result) == 2:
	    sizes = result[1].split()
	elif len(result) ==3:
	    sizes = result[1].split() + result[2].split()
	else:
	    data.error = "len of archive df -h" + path
            break 	   	
        if len(sizes) != 6:
            data.error = "Length of size is not 6" + path
            break
        total, used = list(map(convert_size, sizes[1:3]))
        final = used + source_size
        percent = (final / total) * 100
        print(path, percent)
        if percent <= 80:
	    # a_name = datetime.now().strftime("%Y%md-%H-%m")
	    a_name = data.user + str(datetime.now().date())	
            new_dir = os.path.join( path, a_name)
            #new_dir = new_dir.replace('')
            print(new_dir)
            inp, output, error = ssh.exec_command(f'mkdir {new_dir}')
            print(error.read().decode('utf-8'))
            # os.makedirs(new_dir)
            data.destination_path = new_dir
            cmd = f"chmod 555 {data.source_path};rsync -am --stats --exclude"\
                f" '.snapshot' --log-file=/tmp/archive-dlcm/{a_name}.log {data.source_path} {new_dir}"
            print(cmd)
            inp, output, error = ssh.exec_command(cmd)
            outputs = output.read().decode('utf-8')
            errors = error.read().decode('utf-8')
            print(outputs)
            print(errors)
            data.output = outputs
            data.error = errors
            data.status = f'Completed at {str(datetime.utcnow())}'
	    send_email(data.source_path,data.error)
            session.commit()
            session.close()

            return
    data.status = 'Error'
    if not data.error:
        data.error = 'All destination are full'
    send_email(data.source_path,data.error)
    session.commit()
    session.close()

    return


@migration.route('/job',methods=["POST"])
def migrate():
    data = request.form
    #if data is None:
    #	data = request.form
    print(data)
    #print(type(data))
    data = dict(request.args)
    pb = str(uuid.uuid4())
    url = url_for('status.status_single', pb=pb, _external=True)
    date = datetime.utcnow()
    date_reg = datetime.strptime(data['date_requested'], '%Y-%m-%d')
    data.update({'public_id': pb, 'date_requested': date_reg, 'date': date})
    print(data)
    session = Session()
    val = Data(**data)
    session.add(val)
    session.commit()
    session.close()
    thread = Thread(target=rsync, args=(pb,))
    thread.daemon = True
    thread.start()
    return jsonify({'job_id': pb, 'url': url, 'date': date,
                    'job_status': 'Started'})

@migration.route('/test',methods=["GET", "POST"])
def migratetest():
    data = request.json
    print(data)
    print(type(data))
    return jsonify({'job_id': 121212})