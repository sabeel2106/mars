from sqlalchemy import create_engine, Table, Column,\
    Integer, String, MetaData, Text, Date, DateTime
from sqlalchemy.ext.declarative import declarative_base
import os

URI = 'postgres://postgres:krish.123@localhost:localhost/test'
engine = create_engine(URI)
Base = declarative_base()


class Data(Base):
    __tablename__ = 'data'
    public_id = Column(String, primary_key=True)
    user = Column(String)
    date = Column(DateTime)
    source_path = Column(String)
    destination_path = Column(Text)
    assignee_name = Column(String)
    dept = Column(String)
    mgr = Column(String)
    date_requested = Column(Date)
    bu = Column(String)
    project = Column(String)
    site_of_primary_archive = Column(String)
    status = Column(Text, default='Started')
    output = Column(Text)
    error = Column(Text)
    def __str__(self):
        return f'Created Data {self.public_id}'


if __name__ == '__main__':
    Base.metadata.create_all(engine)
