from flask import Blueprint, request, url_for, jsonify
from flask_cors import cross_orgin, CORS
from sqlalchemy.orm import sessionmaker
from migration.model import Data, engine

status = Blueprint('status', __name__)
CORS(status)
Session = sessionmaker(engine)


@status.route('/')
def status_all():
    session = Session()
    args = dict(request.args)
    data = session.query(Data).filter_by(
        **args).order_by(Data.date).all()
    res = {'result': []}
    for row in data:
        row = row.__dict__
        del row['_sa_instance_state']
        res['result'].append(row)
    session.close()
    return jsonify(res)


@status.route('/<pb>/')
def status_single(pb):
    session = Session()
    data = session.query(Data).filter_by(public_id=pb).first()
    res = data.__dict__
    del res['_sa_instance_state']
    session.close()
    return jsonify(res)
