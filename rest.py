from flask import Flask
from flask_cors import CORS, cross_orgin

# module for blueprints
from migration.migration import migration
from status.status import status

app = Flask(__name__)
CORS(app)

app.register_blueprint(migration, url_prefix='/api/migration/')
app.register_blueprint(status, url_prefix='/api/status/')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
